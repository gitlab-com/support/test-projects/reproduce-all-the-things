# Reproduce all the things

## The things

This project serves two purposes:

- A knowledge dump for reproducing customer issues.  

- Version control for our snippets.

## Guidelines for contributing
Contributions are welcome of course.  They should be based on real examples.

Please use snippets instead of code examples so they can be version controlled.
Then link with `[snippets/snippet-name](snippets/snippet-name)`.

### Create a project with a large number of vulnerabilities

1. Set up a runner with Docker executor and register it to your instance (make sure you have an ultimate license)
2. Import the following project by URL: https://github.com/cr0hn/vulnerable-node (this has a lot of vulnerabilities)
3. Use [snippets/run-sast-ci.yml](snippets/run-sast-ci.yml) as your .gitlab-ci.yml:
4. Let CI run
5. Click Vulnerability Report and there should be a large number unreported vulnerabilities to play with.

### Deploy a Gitaly cluster

[The GDK](https://gitlab.com/gitlab-org/gitlab-development-kit) makes it easy to deploy an arbitrary number of
praefect nodes and virtual storages, without requiring additional infrastructure.

See [GDK: Gitaly and Praefect](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/gitaly.md#gitaly-and-praefect)
for configuration.

### Generate artifacts

Runs fastest with a shell runner.  Use [snippets/generate-artifacts.yml](snippets/generate-artifacts.yml) in `.gitlab-ci.yml`.

If multiple artifacts are required one can add more stages in the pipeine with the same job.  Or create a scheduled job and let it run for a while.

### CI: Push tags, branches, and generate pipelines

Don't run on gitlab.com. Use [snippets/recursive-ci.yml](snippets/recursive-ci.yml) in `.gitlab-ci.yml`.

### Destroy old Pipelines

Rails snippet to destroy Pipelines older than a certain date. Use [snippets/destroy-old-pipelines.rb](snippets/destroy-old-pipelines.rb) in Rails Console.

### De-activate Project Integrations

Rails snippet to de-activate or activate project integrations. Use [snippets/de-activate_project_integrations.rb](snippets/de-activate_project_integrations.rb) in Rails Console.

### Process all `paused` or `hard_failed` mirroring operations

Rails snippet to process mirroring operations in the `paused` or `hard_failed` state. Use [snippets/process-paused-or-hard-failed-mirroring-operations.rb](snippets/process-paused-or-hard-failed-mirroring-operations.rb) in Rails Console.

### Extract push event SHAs from database for a given project

Rails snippet to extract push event SHAs logged in the database for a given project. Use [snippets/extract-push-event-SHAs-for-project.rb](snippets/extract-push-event-SHAs-for-project.rb) in Rails Console.

### Download Docker and deploy a GitLab container

A bash script that can be run on a fresh install of Ubuntu. Accepts version and URL arguments to automate downloading Docker and deploying a GitLab container with the version you provided. Use [snippets/download-docker-and-deploy.sh](snippets/download-docker-and-deploy.sh) in a terminal with the required arguments.

Example: `/bin/sh download-docker-and-deploy.sh-v 14.8.2-ee.0 -u gitlab.example.com`
