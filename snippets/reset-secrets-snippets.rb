table_column_combos = [
  [Namespace, "runners_token_encrypted", "runners_token"],
  [Project, "runners_token_encrypted", "runners_token"],
  [Ci::Build, "token_encrypted", "token"],
  [Ci::Runner, "token_encrypted", "token"],
  [ApplicationSetting, "runners_registration_token_encrypted", "runners_registration_token"],
  [Group, "runners_token_encrypted"],
  [DeployToken, "token_encrypted","token"],
  [User,"static_object_token_encrypted","static_object_token"]
]

table_column_combos.each do |table,column,column2|
  total = 0
  fixed = 0
  removed = 0
  bad = []
  table.find_each do |data|
    begin
      total += 1
      ::Gitlab::CryptoHelper.aes256_gcm_decrypt(data[column])
    rescue => e
      if data[column2].to_s.empty?
        data[column] = nil
        data.save()
        removed += 1
      else
        data[column] = ::Gitlab::CryptoHelper.aes256_gcm_encrypt(data[column2])
        data.save()
        fixed += 1
      end
      bad << data
    end
  end
  puts "Table: #{table.name}    Bad #{bad.length} / Good #{total}, Fixed #{fixed}, Removed #{removed}"
end
