#### Process all "paused" or `hard_failed` mirroring operations

# [Zendesk #244016 (Internal Only)](https://gitlab.zendesk.com/agent/tickets/244016)

# Repository pull mirroring will go into a `hard_failed` state and result in mirroring being paused, when `retry_limit_exceeded` is true.

# - https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/models/ee/project_import_state.rb#L187-190
# - https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/models/ee/project_import_state.rb#L33-36

# `retry_limit_exceeded` is based on the `MAX_RETRY=14`

# - https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/lib/gitlab/mirror.rb#L17


Project.find_each do |p|
  if p.import_state && p.import_state.retry_count >= 14
    puts "Resetting mirroring operation for #{p.full_path}"
    sleep 10
    p.import_state.reset_retry_count
    p.import_state.set_next_execution_to_now(prioritized: true)
    p.import_state.save!
  end
end
